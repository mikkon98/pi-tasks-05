#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NAME_SIZE 320
#define AUTHOR_SIZE 256
struct BOOK
{
	char name[NAME_SIZE];
	char author[AUTHOR_SIZE];
	char age_str[6];
	unsigned int age;
};

int main()
{

	int size = 10;
	int str_count = 0, tab_count = 0, pos = 0;
	char ch;
	char age[5];
	FILE *fp;
	fp = fopen("C:/Users/Vyunov_Rom/Desktop/BOOK.txt", "r");
	if (fp == NULL)
	{
		perror("File: ");
		return 1;
	}
	struct BOOK *shelf = (struct BOOK *)malloc(size* sizeof(struct BOOK));
	if (shelf == NULL)
	{
		puts("Not enough memory!");
		return 11;
	}
	while (1)
	{

		ch = fgetc(fp);
		if (ch == EOF)
		{
			shelf[str_count].age = atoi(shelf[str_count].age_str);
			str_count++;
			break;
		}
		
		if (ch == '\t')
		{
			switch (tab_count)
			{
			case 0:shelf[str_count].name[pos] = '\0';
				break;
			case 1:shelf[str_count].author[pos] = '\0';
				break;
			case 2:shelf[str_count].age_str[pos] = '\0';
				break;
			}
			
			tab_count++;
			pos = 0;
			continue;
		}
		if (ch == '\n')
		{
			shelf[str_count].age = atoi(shelf[str_count].age_str);
			str_count++;
			tab_count = 0;
			pos = 0;

			if (str_count == size)
			{
				shelf = (struct BOOK*)realloc(shelf, (size += 10) * sizeof(struct BOOK));
			}
			continue;
		}
		switch (tab_count)
		{
		case 0:
			shelf[str_count].name[pos] = ch;
			pos++;
			break;
		case 1:
			shelf[str_count].author[pos] = ch;
			pos++;
			break;
		case 2:
			shelf[str_count].age_str[pos] = ch;
			pos++;
			break;
		}
	}
	printf("All Books:\n");
	for (int i = 0; i < str_count; i++)
		printf("%s\t%s\t%d\n", shelf[i].name, shelf[i].author, shelf[i].age);
	putchar('\n');
	int max_pos=0, min_pos=0, max_age=shelf[0].age, min_age=shelf[0].age;
	for (int i = 1; i < str_count; i++)
	{
		if (shelf[i].age > max_age)
		{
			max_pos = i;
			max_age = shelf[i].age;
		}
		if (shelf[i].age < min_age)
		{
			min_pos = i;
			min_age = shelf[i].age;
		}
	}
	printf("Old Book: %s\t%s\t%d\n", shelf[min_pos].name, shelf[min_pos].author, shelf[min_pos].age);
	printf("New Book: %s\t%s\t%d\n", shelf[max_pos].name, shelf[max_pos].author, shelf[max_pos].age);
	putchar('\n');
	printf("Name ascending:\n");
	int i, j;
	struct BOOK tmp;
	for(i=0;i<str_count;i++)
		for(j=str_count-1;j>i;j--)
			if (strcmp(shelf[j - 1].author, shelf[j].author) == 1)
			{
				tmp = shelf[j - 1];
				shelf[j - 1] = shelf[j];
				shelf[j] = tmp;
			}
	for (int i = 0; i < str_count; i++)
		printf("%s\t%s\t%d\n", shelf[i].name, shelf[i].author, shelf[i].age);
	return 0;
}